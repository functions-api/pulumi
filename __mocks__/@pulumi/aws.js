const aws = jest.requireActual('@pulumi/aws')

class Function {
  constructor(name, opts) {
    this.name = name
    this.options = opts
  }
}

aws.lambda.Function = Function

module.exports = aws