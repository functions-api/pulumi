const fs = require('fs')
const path = require('path')
const Function = require('@pulumi/aws').lambda.Function
const Pulumi = require('@pulumi/pulumi')

const functionHandlerString = `
const func = require('./__functions-api-function.js')
module.exports.handler = async function() {
    return await func.handler.apply(func, arguments)
}
`

class LambdaProxyPackager {
  constructor(options) {
    LambdaProxyPackager.checkRequiredOptions(options)

    this.resources = {}
    this.options = options
  }

  static buildOptions(overrides, modulePath) {
    const defaults = {
      runtime: 'nodejs12.x'
    }
    const forcedOptions = {
      handler: '__functions-api-handler.handler'
    }

    let providedOptions = {}
    if (overrides.environmentVariables) {
      providedOptions.environment = { 
        variables: { ...overrides.environmentVariables },
      }
      delete overrides.environmentVariables
    }

    let assets = {
      '__functions-api-handler.js': new Pulumi.asset.StringAsset(functionHandlerString),
      '__functions-api-function.js': new Pulumi.asset.FileAsset(modulePath),
    }

    const nodeModulesPath = path.join(path.dirname(modulePath), 'node_modules')
    try {
      const nodeModulesStat = fs.lstatSync(nodeModulesPath)
      if (nodeModulesStat.isDirectory()) {
        assets['node_modules'] = new Pulumi.asset.FileArchive(nodeModulesPath)
      }
    } catch(e) {
      // noop, this just means node_modules doesn't exist
    }

    if (overrides.assets) {
      Object.keys(overrides.assets).forEach((key) => {
        const path = overrides.assets[key]
        try {
          const stat = fs.lstatSync(path)
          const AssetType = stat.isDirectory() ? Pulumi.asset.FileArchive : Pulumi.asset.FileAsset
          assets[key] = new AssetType(path)
        } catch(e) {
          assets[key] = new Pulumi.asset.StringAsset(path)
        }
      })
      delete overrides.assets
    }
    providedOptions.code = new Pulumi.asset.AssetArchive(assets)

    return {
      ...defaults,
      ...providedOptions,
      ...overrides,
      ...forcedOptions
    }
  }

  static checkRequiredOptions(options) {
    const required = [
      'role',
    ]

    required.forEach((key) => {
      if (typeof(options[key]) === 'undefined') {
        throw new Error(`${this.name} must receive ${key} as an option.`)
      }
    })
  }

  injectResources(toInject) {
    if (typeof(toInject) === 'function') {
      this.resources = {
        ...this.resources,
        ...toInject()
      }
    } else {
      this.resources = {
        ...this.resources,
        ...toInject
      }
    }
  }

  package(handlerPath) {
    const handler = require(handlerPath)

    if (handler.baseType !== 'lambda-proxy') {
      throw new Error('LambdaProxyPackager can only be used with handlers from @functions-api/lambda-proxy.')
    }

    const name = `Lambda-${handler.name}`
    const options = LambdaProxyPackager.buildOptions({
      ...this.options,
      ...handler.getPackageOptions(this.resources),
      handler,
    }, handlerPath)
    return new Function(name, options)
  }
}

module.exports = LambdaProxyPackager