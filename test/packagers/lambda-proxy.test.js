const path = require('path')
const { LambdaProxyPackager } = require('../../lib/index.js')
const BaseHandler = require('@functions-api/lambda-proxy').Handlers.Base

describe('LambdaProxyPackager', () => {
  describe('initialization', () => {
    it ('checks that required options are present', () => {
      const defaultOptions = {
        role: {}
      }
      Object.keys(defaultOptions).forEach((key) => {
        let builder = () => { 
          let options = { ...defaultOptions }
          delete options[key]
          new LambdaProxyPackager(options)
        }
        
        expect(builder).toThrow(key)
      })
    })
  })

  describe('buildOptions', () => {
    const requiredOptions = {
      role: {}
    }

    it ('sets the runtime to node12 by default', () => {
      const options = LambdaProxyPackager.buildOptions(requiredOptions, __filename)
      expect(options.runtime).toEqual('nodejs12.x')
    })

    it('forces the handler function', () => {
      const options = LambdaProxyPackager.buildOptions(requiredOptions, __filename)
      expect(options.handler).toEqual('__functions-api-handler.handler')
    })

    it('sets environment variables', () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        environmentVariables: {
          test: true
        }
      }, __filename)

      expect(options.environment.variables.test).toBe(true)
    })

    it('does not set environmentVariables', () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        environmentVariables: {
          test: true
        }
      }, __filename)

      expect(options).not.toHaveProperty('environmentVariables')
    })

    it('creates the handler and function assets', async () => {
      const options = LambdaProxyPackager.buildOptions(requiredOptions, __filename)
      const assets = await options.code.assets

      expect(assets['__functions-api-handler.js'].constructor.name).toEqual('StringAsset')
      expect(assets['__functions-api-function.js'].constructor.name).toEqual('FileAsset')
    })

    it('includes node_modules as assets', async() => {
      const testHandlerPath = path.join(__dirname, '../', 'test-handler', 'index.js')
      const options = LambdaProxyPackager.buildOptions(requiredOptions, testHandlerPath)
      const assets = await options.code.assets

      expect(assets['node_modules'].constructor.name).toEqual('FileArchive')
    })

    it('creates file assets', async () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        assets: {
          'test.js': path.join(__dirname, '../../lib/index.js')
        }
      }, __filename)
      const assets = await options.code.assets

      expect(assets['test.js'].constructor.name).toEqual('FileAsset')
    })

    it('creates directory assets', async () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        assets: {
          'test': path.join(__dirname, '../../lib/')
        }
      }, __filename)
      const assets = await options.code.assets

      expect(assets.test.constructor.name).toEqual('FileArchive')
    })

    it('creates string asseets', async () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        assets: {
          'test': 'Hello, world!'
        }
      }, __filename)
      const assets = await options.code.assets

      expect(assets.test.constructor.name).toEqual('StringAsset')
      expect(assets.test.text).resolves.toEqual('Hello, world!')
    })

    it('does not set the assets key', () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        assets: {
          test: 'Hello, world!'
        }
      }, __filename)

      expect(options).not.toHaveProperty('assets')
    })

    it('passes on other options', () => {
      const options = LambdaProxyPackager.buildOptions({
        ...requiredOptions,
        test: true
      }, __filename)

      expect(options.test).toBe(true)
    })
  })

  describe('injectResources', () => {
    let packager;
    beforeEach(() => {
      packager = new LambdaProxyPackager({
        role: {}
      })
    })

    it ('sets resources from objects', () => {
      packager.injectResources({ test: true })
      expect(packager.resources.test).toBe(true)
    })

    it('sets resources from functions', () => {
      packager.injectResources(() => {
        return { test: true }
      })

      expect(packager.resources.test).toBe(true)
    })

    it('overrides previously set resources', () => {
      packager.injectResources({ 
        first: 'one',
        second: 'two'
      })

      packager.injectResources({ first: 'foo' })
      expect(packager.resources.first).toEqual('foo')
      expect(packager.resources.second).toEqual('two')

      packager.injectResources(() => {
        return { second: 'bar' }
      })
      expect(packager.resources.first).toEqual('foo')
      expect(packager.resources.second).toEqual('bar')
    })
  })

  describe('package', () => {
    let packager;
    const handlerPath = path.join(__dirname, '../', 'test-handler', 'index.js')
    const handler = require(handlerPath)
    beforeEach(() => {
      packager = new LambdaProxyPackager({
        role: {},
        override: 'value'
      })
      handler.getPackageOptions.mockReset()
    })

    it('sets the name', () => {
      const func = packager.package(handlerPath)
      expect(func.name).toBe('Lambda-Handler')
    })

    it('passes resources to the handler packager', () => {
      packager.injectResources({ test: true })
      packager.package(handlerPath)
      expect(handler.getPackageOptions).toHaveBeenCalledWith({ test: true })
    })

    it('combines packager options and handler-packaged options', () => {
      handler.getPackageOptions.mockReturnValueOnce({
        first: 'one',
        second: 'two'
      })

      const func = packager.package(handlerPath)
      expect(func.options.first).toEqual('one')
      expect(func.options.second).toEqual('two')
      expect(func.options.override).toEqual('value')
    })

    it('creates a Pulumi Function', () => {
      const aws = require('@pulumi/aws')
      const func = packager.package(handlerPath)
      expect(func).toBeInstanceOf(aws.lambda.Function)
    })

    it('checks that the handler is a LambdaProxy handler', () => {
      const prevType = handler.baseType
      handler.baseType = 'test'
      const create = () => { packager.package(handlerPath) }
      expect(create).toThrow('@functions-api/lambda-proxy')

      handler.baseType = prevType
    })
  })
})