const BaseHandler = require('@functions-api/lambda-proxy').Handlers.Base

class Handler extends BaseHandler { }
Handler.prototype.run = jest.fn()
Handler.getPackageOptions = jest.fn().mockReturnValue()

module.exports = Handler